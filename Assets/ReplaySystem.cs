﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReplaySystem : MonoBehaviour {

    //wherever you can use a constant instead of a variable, do it
    //Since set already, unlike an int, we can initialize our array right away.
    private const int bufferFrames = 200; //if were like 1000 we'd need to think about how we'd deal with the case where we'd like to replay before we've even written all the frames 1x
    private MyKeyFrame[] keyFrames = new MyKeyFrame[bufferFrames]; //if bufferframes were an int, the initializer cannot reference a nonstatic field 
    private Rigidbody myRigidbody;
    private GameManager gameManager;
	// Use this for initialization
	void Start () {
        myRigidbody = this.GetComponent<Rigidbody>();
        gameManager = GameObject.FindObjectOfType<GameManager>();
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (gameManager.recording)
        {
            Record();
        }
        else
        {
            PlayBack();
        }

    }
    private void Record()
    {
        myRigidbody.isKinematic = false; //physics engine moves the object (unlike in PlayBack)
        int frame = Time.frameCount % bufferFrames; // number of total frames since the game started % buffer
        float time = Time.time;
        Debug.Log("Writing frame " + frame + " to buffer");
        keyFrames[frame] = new MyKeyFrame(time, this.transform.position, transform.rotation);
    }

    private void PlayBack()
    {
        myRigidbody.isKinematic = true; // we don't want the physics engine driving its move b/c we're moving it ourselves
        int frame = Time.frameCount % bufferFrames; //if we were on frame % = 9 when we stopped recording, 
                                                    //we'd start playing back at the right place in PlayBack n/c next frame in our buffer is the oldest 
                                                    //frame we have (the next that would've been erased)
        //could play backwards then forwards. 
        Debug.Log("Reading frame " + frame + " from buffer");
        transform.position = keyFrames[frame].position;
        transform.rotation = keyFrames[frame].rotation;
    }


}



/// <summary>
/// A structure for storing time, position and rotation.
/// </summary>
public struct MyKeyFrame
{
    //we're just storing values, so we're fine to store like this.
    //if we had class instead of struct it'd work, and construct/initialize, the same

    private float time;
    public Vector3 position;
    public Quaternion rotation;

    public MyKeyFrame(float time, Vector3 position, Quaternion rotation)
    {
        this.time = time;
        this.position = position;
        this.rotation = rotation;
    }

     
}
