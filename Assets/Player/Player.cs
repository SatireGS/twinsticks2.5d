﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

public class Player : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        //see InputManager (Edit>Project Settings>Input) for what's been defined
        Debug.Log("Horizontal: " + CrossPlatformInputManager.GetAxis("Horizontal"));
        //horizontal has a property called Gravity (3 = defualt) in InputManager
        //this simulates the left/right arrows acting more like a controller, not 
        //returning to 0 immediately so if you hold down right arrow it'll hit .99,
        //but when you let go it doesn't go straight to 0, rather it gradually goes
        //to 0. The speed that this happens is defined by Gravity. 
        Debug.Log("Vertical: " + CrossPlatformInputManager.GetAxis("Vertical"));
        //You can also allow your player to change these settings. Straight out of
        //the box, when you build and run the executable the player can customize the game.


    }
}
